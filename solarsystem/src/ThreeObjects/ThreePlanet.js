import * as THREE from "three";

const radiusShift = 0.05;
const nbSegments = 32;

export const ThreePlanetParams = {
    isSphereVisible: false,
};

export class ThreePlanet extends THREE.Object3D {

    constructor(name, radius ) {
        super();

        this._name = name;
        this._radius = radius;
        this._geometry = new THREE.SphereGeometry( radius, nbSegments, nbSegments );
       
        this._material = new THREE.MeshPhongMaterial( 
            { 
                bumpScale: 0.08,
                transparent: true,
                depthWrite: false,                
            }
        );

        this._mesh =  new THREE.Mesh( this._geometry, this._material );
        this.add( this._mesh );
    }

    setSphereVisible( yn ) {
        this._mesh.visible = yn;
    }

    geometry() {
        return this._geometry;
    }
    
    setBackSideOn( yn ) {
        this._material.side = yn ? THREE.BackSide : null;
    }

    uiName() {
        return this._name;
    }

    radius() {
        return this._radius;
    }

    static loadTexture(url) {
        const loader = new THREE.TextureLoader();
        return loader.load(url);
        //TODO promise
    }

    setUpTexture(img) {
        const tex = ThreePlanet.loadTexture( img );
        this._material.map = tex;
    }

    addRelief(img) {
        const bumpTex = ThreePlanet.loadTexture(img);
        this._material.bumpMap = bumpTex; 
    }

    addSpecular(img) {
        this._material.specularMap = ThreePlanet.loadTexture(img);
        this._material.specular = new THREE.Color('grey');
    }

    addAtmosphere(img) {
        const geometry = new THREE.SphereGeometry( this.radius()+radiusShift, nbSegments, nbSegments );
        const tex = ThreePlanet.loadTexture(img);
        const material = new THREE.MeshPhongMaterial();
        material.map = tex;
        material.transparent = true;
        const mesh = new THREE.Mesh(geometry, material);
        this.add(mesh)
    }
}