import * as THREE from "three";

export class ThreeStarField extends THREE.Object3D {

    constructor(number,radius=1500) {
        super();

        const geom = new THREE.Geometry();
        for ( let idx=0; idx<number; idx++ ) {
          const lat = 2*Math.PI*Math.random();
          const lon = 2*Math.PI*Math.random();
          const vertex = new THREE.Vector3(
             Math.cos(lon), Math.sin(lat), 2*Math.random()-1 );
          vertex.multiplyScalar( radius );
          geom.vertices.push( vertex );
        }
        const mat = new THREE.PointsMaterial({color: 0xffffff,size: 1});
        const sys = new THREE.Points(geom, mat);
        this.add(sys);
    }
}