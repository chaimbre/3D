import {ThreePlanet,ThreePlanetParams} from "./ThreePlanet"
import { Lensflare, LensflareElement } from 'three/examples/jsm/objects/Lensflare.js';
import * as THREE from "three";

export const ThreePlanetLightSourceParams = {
    ...ThreePlanetParams,
    LensFlareSize: 500,
    LightIntensity: 5.0,
};

export class ThreePlanetLightSource extends ThreePlanet {
    constructor(name, radius) {
        super(name, radius);

        this._radius = this.radius();
        this._lensFlare = null;
        this._lensFlareSize = 500;
        this._glowMaterial = null;
        this._glowMesh = null;
        this._light = new THREE.PointLight( 0xffaacc, 1 );
        this.add(this._light);
        
        this.addLensFlare(this._lensFlareSize);
    }

    addLensFlare(size) {
        if ( this._lensFlare ) {
            this._lensFlare.dispose();
            this.remove( this._lensFlare );
        }

        this._lensFlareSize = size;
        const tex = ThreePlanet.loadTexture( `../Files/${this._name}lensflare.png` );
        const flareColor = new THREE.Color(0xffaacc);

        this._lensFlare = new Lensflare();
        this._lensFlare.addElement( new LensflareElement(tex, size, 0, flareColor ) );
        const tex2 = ThreePlanet.loadTexture( `../Files/${this._name}lensflare2.png` );
        this._lensFlare.addElement( new LensflareElement(tex2, 60, 0.6 ));
        this._lensFlare.addElement( new LensflareElement(tex2, 70, 0.7) );
        this._lensFlare.addElement( new LensflareElement(tex2, 120, 0.9) );
        this._lensFlare.addElement( new LensflareElement(tex2, 180, 1.0) );
        this.add(this._lensFlare);
    }

    setLensFlareSize(size) {
        if (this._lensFlareSize !== size)
            this.addLensFlare(size);
    }

    setLightIntensity(val) {
        if (this._light)
            this._light.intensity = val;
    }
}
