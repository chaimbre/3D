import {ThreePlanet} from "./ThreePlanet"
import {lightFragmentShader, lightVertexShader} from "./Shaders"
import * as THREE from "three";
import { Vector3 } from "three";

export const ThreePlanetGlowParams = {
    ...ThreePlanetParams,
    GlowIntensity: 6.0,
}

export class ThreePlanetGlow extends ThreePlanet {
    constructor(name, radius) {
        super(name, radius);

        this._radius = this.radius();
        this._glowMaterial = null;
        this._glowMesh = null;
    
        this.addGlow();
    }
    
    addGlow() {
        this._glowMaterial = new THREE.ShaderMaterial({
            uniforms: {
                viewVector: {
                    type: "v3",
                    value: new Vector3(0,0,0),
                },
                intensityFactor: {
                    value : 6.0
                }
            },
            vertexShader: lightVertexShader(),
            fragmentShader: lightFragmentShader(),
            side: THREE.FrontSide,
            blending: THREE.AdditiveBlending,
            transparent: true
        });
        const geometry = new THREE.SphereGeometry(this._radius, 32, 32);
        this._glowMesh = new THREE.Mesh(geometry, this._glowMaterial);
        this.add(this._glowMesh);

        return this._glowMesh;
    }

    setGlowIntensity(val) {
        if (this._glowMaterial)
            this._glowMaterial.uniforms.intensityFactor.value = val;
    }

    setPos(pos) {
        let worldPos = new THREE.Vector3();
        this._glowMesh.getWorldPosition(worldPos);
        let viewVector = new THREE.Vector3().subVectors( pos, worldPos );
        this._glowMaterial.uniforms.viewVector.value = viewVector;
    }
}
