
export function lightVertexShader() {
    return `
        uniform vec3 viewVector;
        uniform float intensityFactor;
        varying float intensity;
        void main() {
            gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4( position, 1.0 );
            vec3 actual_normal = vec3(modelMatrix * vec4(normal, 0.0));
            intensity = pow( dot(normalize(viewVector), actual_normal), intensityFactor);
        }`
}

export function lightFragmentShader() {
    return ` 
        varying float intensity;
        void main() {
            vec3 glow = vec3(1, 1, 1) * intensity;
            gl_FragColor = vec4( glow, 1.0 );
        }`
    }