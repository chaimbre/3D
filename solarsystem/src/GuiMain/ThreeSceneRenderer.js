import PubSub, {PubSubEvents} from '../Basic/PubSub'

import * as THREE from "three";
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';

export class ThreeSceneRenderer {

    constructor() {
        this._scene = new THREE.Scene();
        this._camera = new THREE.PerspectiveCamera( 45, 1, 0.1, 1000 );
        this._camera.position.y = 20;
        
        this._renderer = new THREE.WebGLRenderer();
        this._controls = null;
        this._requestedAnimationID = -1;

        this._ambientLight = new THREE.AmbientLight( 0xffffff, 1.0);
        this._scene.add(this._ambientLight);

        this.initControl();
    }

    setAmbiantLightIntensity( val ) {
        this._ambientLight.intensity = val;
    }

    initControl() {
        this._controls = new OrbitControls( this._camera, this.domElement() );
        
        const reqRender = () => this.requestRender();
        this._controls.addEventListener('change', 
            () => PubSub.publish( PubSubEvents.CAMERA_CHANGED, {'position': this._camera.position.clone()}) );
        this._controls.addEventListener('change', reqRender);
        this._controls.addEventListener('start', reqRender);
        this._controls.addEventListener('end', reqRender);
    }

    requestRender() {
        const self = this;
        self._requestedAnimationID && cancelAnimationFrame(self._requestedAnimationID);
        this._requestedAnimationID = requestAnimationFrame( () => {
          
            console.log( "render"); //TODO change this to a log mgr

            if ( self._controls )
                self._controls.update();
            self._renderer.render( self._scene, self._camera );
        });
    }

    setSize(w,h) {
        this._renderer.setSize(w,h);
        this._camera.aspect = w / h;;
        this._camera.updateProjectionMatrix();

        this.requestRender();
    }

    domElement() {
        return this._renderer.domElement;
    }

    scene() {
        return this._scene;
    }
}