import {ThreeGeneralParams} from '../ThreeObjects/ThreeParams'
import {ThreePlanet, ThreePlanetParams} from '../ThreeObjects/ThreePlanet'
import {ThreePlanetLightSource, ThreePlanetLightSourceParams} from '../ThreeObjects/ThreePlanetLightSource'
import {ThreeSceneRenderer} from './ThreeSceneRenderer'
import {ThreeStarField} from '../ThreeObjects/ThreeStars'

import PubSub, {PubSubEvents} from '../Basic/PubSub'
import {fetchPlanetData,fetchPlanetTexture} from '../Basic/APIUtils'
import {GUI} from "three/examples/jsm/libs/dat.gui.module"


export class MainBuilder {
    
    constructor(scene) {
        this._sceneRenderer = new ThreeSceneRenderer();
        this._scene = this._sceneRenderer.scene();
        
        this._planets = [];
        this._planetpars = [];
        this._threegeneralpar = ThreeGeneralParams;
        this._gui = new GUI();
    }

    sceneRenderer() {
        return this._sceneRenderer;
    }

    init() {
        console.log( "Init builder...");
       
        this.addSolarSystem();

        this.initDatGui();

        this._sceneRenderer.requestRender();

        PubSub.subscribe( PubSubEvents.CAMERA_CHANGED, evt => this.onCameraChange(evt.position) );
    }

    async addSolarSystem() {
        const sun = new ThreePlanetLightSource( "sun", 4 );
        this._scene.add( sun );
        this.addplanetGUI(sun); 
        
        const planets = await fetchPlanetData();
        await Promise.all( 
            planets.map( pl => this.addplanet( sun, pl ) )
             //  this.addplanet( earth, "moon", 6*27/100, -10 );
        );
       // this.addplanetGUI(earth);

        const starfield = new ThreeStarField( 1e4 );
        this._scene.add( starfield );
    }

    initDatGui()  {
        const visParChg = () => this.onThreeGeneralParChanged();
        const generalGui = this._gui.addFolder( "General" );
        generalGui.add(this._threegeneralpar, "ambientLightIntensity", 0, 1, 0.1).onChange(visParChg);
    }

    onThreeGeneralParChanged() {
        const vp = this._threegeneralpar;
        this._sceneRenderer.setAmbiantLightIntensity(vp.ambientLightIntensity);
        this._sceneRenderer.requestRender();
    }

    onplanetDispChanged( planet, pp ) {
        planet.setSphereVisible(pp.isSphereVisible);
        const isLightSource = planet instanceof ThreePlanetLightSource;
        if ( isLightSource ) {
            planet.setLensFlareSize(pp.LensFlareSize);
            planet.setLightIntensity(pp.LightIntensity);
        }
        this._sceneRenderer.requestRender();
    } 

    async addplanet( parent, { englishName, meanRadius, perihelion } ) {
        const name = englishName;
        const radius = this._threegeneralpar._planetScale;
        const pl = new ThreePlanet(name,radius);
        parent.add( pl );

        this._planets.push( { name, pl } );
        pl.position.x = this._threegeneralpar.distanceScale*perihelion;

        const tex_img = await fetchPlanetTexture( name );
        if( tex_img )
            pl.setUpTexture( tex_img );
        
        return pl;
    }

    addplanetGUI(planet) {
        if ( !planet )
            return;

        const isLightSource = planet instanceof ThreePlanetLightSource;
        const pars = isLightSource ? ThreePlanetLightSourceParams : ThreePlanetParams;

        this._planetpars.push( pars );

        const onDispChanged = () => this.onplanetDispChanged(planet,pars);
        const plGui = this._gui.addFolder( planet.uiName() );
        plGui.add( pars, 'isSphereVisible' ).onChange(onDispChanged);

        if ( isLightSource ) {
            plGui.add( pars, 'LensFlareSize', 1, 1000, 100).onChange(onDispChanged);
            plGui.add( pars, 'LightIntensity', 1, 25, 1).onChange(onDispChanged);
        }
    }

    onCameraChange(pos) {
        //TODO
    }

    readFromJson() {
        //TODO
    }

    savetoJson() {
        //TODO
    }
}
