import React, {Component} from "react";

export class GuiScene3D extends Component {

    constructor(props) {
      super();

      this._sceneRenderer = props.sceneRenderer;
    }

    componentDidMount() {
        // document.body.appendChild( renderer.domElement );
        // use ref as a mount point of the Three.js scene instead of the document.body
        this._sceneRenderer.setSize( window.innerWidth, window.innerHeight );
        this.mount.appendChild( this._sceneRenderer.domElement() );
      }

      render() {
        return (
          <div ref={ref => (this.mount = ref)} />
        )
      }
}