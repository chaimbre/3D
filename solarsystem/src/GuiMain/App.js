import './App.css';
import {useEffect} from 'react'
import {GuiScene3D} from './GuiScene3D';
import {MainBuilder} from './GuiMainBuilder'


const mainBuilder = new MainBuilder();
mainBuilder.init();

function App() {

  useEffect(
    () => {
        window.addEventListener("resize", evt => onWindowResize(evt), false);
        return () => {
            window.removeEventListener("resize", evt => onWindowResize(evt), false)
        }
    }, []
  );
  
  function onWindowResize(evt) {
    const width = window.innerWidth;
    const height = window.innerHeight;

    mainBuilder.sceneRenderer().setSize(width,height);
  }


  return (
    <div className="3DSolarSystem">
        <GuiScene3D sceneRenderer={mainBuilder.sceneRenderer()}/>
    </div>
  );
}

export default App;
