const PATH_BASE = 'https://api.le-systeme-solaire.net/';
const PATH_SEARCH = 'rest/';
const PARAM_SEARCH = 'bodies';
const QUERY = '';

const TEX_PATH_BASE = 'https://api.github.com/repos/'
const TEX_GIT_REPO = 'nasa/NASA-3D-Resources/'
const TEX_PATH_SEARCH = 'Images%20and%20Textures/'
const TEX_PARAM_SEARCH = '';
const TEX_QUERY = '';

const C_MIN_RADIUS = 2000;

export async function getLocalUrl(name) {
    const url = `../Files/${name}.png`;
    return url;
}

export function getPlanetDataUrl(searchTerm = QUERY) {
    const url = `${PATH_BASE}${PATH_SEARCH}${PARAM_SEARCH}${searchTerm}`;
    return url;
}

export async function fetchPlanetData(minradius = C_MIN_RADIUS) {
    const url = getPlanetDataUrl();
    const req = await fetch(url);
    const data = await (req.json());
    return data.bodies.filter(elt => elt.isPlanet && elt.meanRadius > C_MIN_RADIUS);
}

export function getTexUrl(searchTerm = TEX_QUERY) {
    const url = `${TEX_PATH_BASE}${TEX_GIT_REPO}${'contents/'}${TEX_PATH_SEARCH}${TEX_PARAM_SEARCH}${searchTerm}`;
    return url;
}

export async function fetchPlanetTexture(name, errmsg) {
    const url = getTexUrl(name);
    let res = null;
    try {
        res = await fetch(url);
    } catch (err) {
        console.log(err)
    }
    if ( !res.ok ) {
        console.log(`\n API file not found. Trying local ${name}.png in Files folder...`);
        return getLocalUrl(name.toLowerCase());
    }

    const data = res ? await res.json() : null;
    if (data && data.length && data[0]) {
        const img = await fetch(data[0].download_url);
       
        if (img) {
            const blob_img = await img.blob();
            return URL.createObjectURL(blob_img);
        }
    }
    return null;
}