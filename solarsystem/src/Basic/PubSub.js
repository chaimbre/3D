import PubSubLib from 'pubsub-js';

export const PubSubEvents = { CAMERA_CHANGED: 'camera_changed' };

export default {

    publish: function(topic, event) {
        PubSubLib.publish(topic, event);
    },

    subscribe: function(topic, subscriber) {
        PubSubLib.subscribe(topic, (t, e) => {
            subscriber.call(null, e);
        });
    },

    unsubscribe: function(token) {
        PubSubLib.unsubscribe(token);
    },
}
