#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLayout>
#include <QFileDialog>

#include "mainwidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionImage,&QAction::triggered,this,&MainWindow::onImageLoaded);
    connect(ui->sliderExageration,&QSlider::valueChanged,this,
            [&](int v){ui->mainWidget_->setExageration(v);});
    connect(ui->sliderShapeSize,&QSlider::valueChanged,this,
            [&](int v){ui->mainWidget_->setShapeSize(v);});

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onImageLoaded()
{
    QString fileName = QFileDialog::getOpenFileName(this,
           tr("Open Image"), "",
           tr("Pictures (*.png);;All Files (*)"));

    if ( !fileName.isEmpty() )
        ui->mainWidget_->setImage( fileName.toStdString().c_str() );
}

