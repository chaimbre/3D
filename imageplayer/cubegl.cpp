#include "cubegl.h"

#include <QVector2D>
#include <QVector3D>
#include <QOpenGLShaderProgram>

CubeGL::CubeGL()
{
    init();
}

void CubeGL::init()
{
    std::vector<QVector3D> vertices = {
        // Vertex data for face 0
        QVector3D(-1.0f, -1.0f,  1.0f),  // v0
        QVector3D( 1.0f, -1.0f,  1.0f), // v1
        QVector3D(-1.0f,  1.0f,  1.0f),  // v2
        QVector3D( 1.0f,  1.0f,  1.0f), // v3

        // Vertex data for face 1
        QVector3D( 1.0f, -1.0f,  1.0f),  // v4
        QVector3D( 1.0f, -1.0f, -1.0f),  // v5
        QVector3D( 1.0f,  1.0f,  1.0f),   // v6
        QVector3D( 1.0f,  1.0f, -1.0f),  // v7

        // Vertex data for face 2
        QVector3D( 1.0f, -1.0f, -1.0f),  // v8
        QVector3D(-1.0f, -1.0f, -1.0f),  // v9
        QVector3D( 1.0f,  1.0f, -1.0f),  // v10
        QVector3D(-1.0f,  1.0f, -1.0f),   // v11

        // Vertex data for face 3
        QVector3D(-1.0f, -1.0f, -1.0f), // v12
        QVector3D(-1.0f, -1.0f,  1.0f),   // v13
        QVector3D(-1.0f,  1.0f, -1.0f),  // v14
        QVector3D(-1.0f,  1.0f,  1.0f),   // v15

        // Vertex data for face 4
        QVector3D(-1.0f, -1.0f, -1.0f),  // v16
        QVector3D( 1.0f, -1.0f, -1.0f),  // v17
        QVector3D(-1.0f, -1.0f,  1.0f),  // v18
        QVector3D( 1.0f, -1.0f,  1.0f), // v19

        // Vertex data for face 5
        QVector3D(-1.0f,  1.0f,  1.0f),  // v20
        QVector3D( 1.0f,  1.0f,  1.0f),  // v21
        QVector3D(-1.0f,  1.0f, -1.0f),  // v22
        QVector3D( 1.0f,  1.0f, -1.0f), // v23
    };

    std::vector<QVector2D> tex = {
        QVector2D(0.0f, 0.0f),
        QVector2D(0.33f, 0.0f),
        QVector2D(0.0f, 0.5f),
        QVector2D(0.33f, 0.5f),

        QVector2D( 0.0f, 0.5f),
        QVector2D(0.33f, 0.5f),
        QVector2D(0.0f, 1.0f),
        QVector2D(0.33f, 1.0f),

        QVector2D(0.66f, 0.5f),
        QVector2D(1.0f, 0.5f),
        QVector2D(0.66f, 1.0f),
        QVector2D(1.0f, 1.0f),

        QVector2D(0.66f, 0.0f),
        QVector2D(1.0f, 0.0f),
        QVector2D(0.66f, 0.5f),
        QVector2D(1.0f, 0.5f),

        QVector2D(0.33f, 0.0f),
        QVector2D(0.66f, 0.0f),
        QVector2D(0.33f, 0.5f),
        QVector2D(0.66f, 0.5f),

        QVector2D(0.33f, 0.5f),
        QVector2D(0.66f, 0.5f),
        QVector2D(0.33f, 1.0f),
        QVector2D(0.66f, 1.0f)
    };


    std::vector<GLushort> indices = {
         0,  1,  2,  3,  3,     // ( v0,  v1,  v2,  v3)
         4,  4,  5,  6,  7,  7, // ( v4,  v5,  v6,  v7)
         8,  8,  9, 10, 11, 11, // ( v8,  v9, v10, v11)
        12, 12, 13, 14, 15, 15, // (v12, v13, v14, v15)
        16, 16, 17, 18, 19, 19, // (v16, v17, v18, v19)
        20, 20, 21, 22, 23      // (v20, v21, v22, v23)
    };

    bindAndAllocateVBOs(vertices,indices);
    bindAndAllocateVBO(tex,texBuf_);
}

