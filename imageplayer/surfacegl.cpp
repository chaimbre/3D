#include "surfacegl.h"

#include <QImage>
#include <QVector2D>
#include <QVector3D>
#include <QOpenGLShaderProgram>

#include <iostream>

SurfaceGL::SurfaceGL(const ImageData& id)
    : imageData_(id)
{
    init();
}

void SurfaceGL::init()
{
    const QImage& image = imageData_.image_;
    const double sizex = image.width();
    const double sizey = image.height();
    int exageration = imageData_.exageration_;
    int shapeSize = imageData_.shapeSize_;
    if ( !sizex || !sizey )
        return;

    std::vector<QVector3D> vertices(sizex*sizey);
    std::vector<QVector2D> texcoords(sizex*sizey);
    std::vector<GLushort> indices(6*(sizex-1)*(sizey-1));
    for ( size_t idy=0; idy<sizey; idy++ )
    {
        for ( size_t idx=0; idx<sizex; idx++ )
        {
            QRgb rgb = image.pixel(idx,idy);
            size_t index = idy*sizex + idx;
            double vx = shapeSize*idx/sizex-shapeSize/2;
            double vy = exageration*qGray(rgb)/(double)255;
            double vz = shapeSize*idy/sizey-shapeSize/2;
            vertices[index] = QVector3D( vx, vy, vz );

            texcoords[index] = QVector2D( vx/(sizex-1), 1.0-vy/(sizey-1));
        }
    }

    for ( size_t idy=0; idy<sizey-1; idy++ )
    {
        for ( size_t idx=0; idx<sizex-1; idx++ )
        {
            size_t ver_index = idy*sizex + idx;
            size_t index = 6*(idy*(sizex-1) + idx);
            indices[index]   = ver_index;
            indices[index+1] = ver_index+sizex;
            indices[index+2] = ver_index+1;
            indices[index+3] = ver_index+1;
            indices[index+4] = ver_index+sizex;
            indices[index+5] = ver_index+1+sizex;
        }
    }
    bindAndAllocateVBOs(vertices,indices);
    bindAndAllocateVBO(texcoords,texBuf_);
}
