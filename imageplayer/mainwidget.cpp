#include "mainwidget.h"
#include "cubegl.h"
#include "surfacegl.h"
#include "geometryengine.h"

#include <QMouseEvent>

#include <cmath>
#include <iostream>

MainWidget::MainWidget( QWidget* par )
    : QOpenGLWidget(par)
{}


MainWidget::~MainWidget() = default;


void MainWidget::mousePressEvent(QMouseEvent* e)
{
    mousePressPosition_ = QVector2D(e->localPos());
}

void MainWidget::mouseMoveEvent(QMouseEvent* e)
{
    QVector2D diff = QVector2D(e->localPos()) - mousePressPosition_;
    QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();
    qreal acc = diff.length() / 100.0;
    rotationAxis_ = (rotationAxis_ + n * acc).normalized();

    rotation_ *= QQuaternion::fromAxisAndAngle(rotationAxis_, 1);

    update();
}

void MainWidget::mouseReleaseEvent(QMouseEvent* )
{}

void MainWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0, 0, 0, 1);

    initShaders();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    imageData_ = std::make_unique<ImageData>();
    dataGL_ = std::make_unique<SurfaceGL>(*imageData_.get()); //std::make_unique<SurfaceGL>(*imageData_.get());
    dataGL_->setProgram(&program_);
}


void MainWidget::initShaders()
{
    if (!program_.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vshader.glsl"))
        close();

    if (!program_.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fshader.glsl"))
        close();

    if (!program_.link())
        close();

    if (!program_.bind())
        close();
}


void MainWidget::setImage(const char* path )
{
    QImage img = path ? QImage(path).mirrored() : QImage(1,1,QImage::Format_Mono);
    imageData_->image_ = img;

    texture_ = std::make_unique<QOpenGLTexture>(img);
    initTextures();

    dataGL_->init();
    update();
}


void MainWidget::setExageration(int ex)
{
    imageData_->exageration_ = ex;
    dataGL_->init();
    update();
}

void MainWidget::setShapeSize( int sz )
{
    imageData_->shapeSize_ = sz;
    dataGL_->init();
    update();
}

void MainWidget::initTextures()
{
    if (texture_) {
        texture_->setMinificationFilter(QOpenGLTexture::Nearest);
        texture_->setMagnificationFilter(QOpenGLTexture::Linear);
        texture_->setWrapMode(QOpenGLTexture::Repeat);
    }
    program_.setUniformValue("texture", 0);
}


void MainWidget::resizeGL(int w, int h)
{
    qreal aspect = qreal(w) / qreal(h ? h : 1);
    const qreal zNear = 3.0, zFar = 7.0, fov = 45.0;
    projection_.setToIdentity();
    projection_.perspective(fov, aspect, zNear, zFar);
}


void MainWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if ( texture_)
        texture_->bind();

    QMatrix4x4 matrix;
    matrix.translate(0.0, 0.0, -5.0);
    matrix.rotate(rotation_);
    program_.setUniformValue("mvp_matrix", projection_ * matrix);

    dataGL_->draw();
}

