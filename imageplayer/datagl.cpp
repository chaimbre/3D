#include "datagl.h"

#include <QVector2D>
#include <QVector3D>
#include <QOpenGLShaderProgram>

DataGL::DataGL()
    : indexBuf_(QOpenGLBuffer::IndexBuffer)
{
    initializeOpenGLFunctions();

    arrayBuf_.create();
    indexBuf_.create();
    texBuf_.create();
}

DataGL::~DataGL()
{
    arrayBuf_.destroy();
    indexBuf_.destroy();
    texBuf_.destroy();
}

void DataGL::draw()
{
    if (!program_)
        return;

    arrayBuf_.bind();
    int vertexLocation = program_->attributeLocation("a_position");
    program_->enableAttributeArray(vertexLocation);
    program_->setAttributeBuffer(vertexLocation, GL_FLOAT, 0, 3, sizeof(QVector3D));

    texBuf_.bind();
    int texcoordLocation = program_->attributeLocation("a_texcoord");
    program_->enableAttributeArray(texcoordLocation);
    program_->setAttributeBuffer(texcoordLocation, GL_FLOAT, 0, 2,sizeof(QVector2D));

    indexBuf_.bind();
    glDrawElements(GL_TRIANGLE_STRIP, indexBuf_.size(), GL_UNSIGNED_SHORT, nullptr);
}
