#pragma once

#include <QMainWindow>
#include <QString>

class MainWidget;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
            MainWindow(QWidget *parent = nullptr);
            ~MainWindow();

private slots:
    void    onImageLoaded();

private:
    Ui::MainWindow* ui;

    MainWidget*   mainWidget_;
};


