QT       += core gui widgets


TEMPLATE = app

SOURCES += main.cpp \
    cubegl.cpp \
    datagl.cpp \
    mainwidget.cpp \
    mainwindow.cpp \
    surfacegl.cpp

HEADERS += \
    cubegl.h \
    datagl.h \
    mainwidget.h \
    mainwindow.h \
    surfacegl.h

RESOURCES += \
    shaders.qrc

FORMS += \
    mainwindow.ui

DISTFILES += \
    fshader.glsl \
    shader.pro.user \
    shader_en_150.ts \
    vshader.glsl

