#pragma once

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

class QOpenGLShaderProgram;

class GeometryEngine : protected QOpenGLFunctions
{
public:
    GeometryEngine();
    virtual ~GeometryEngine();

    void draw();

    void            setProgram(QOpenGLShaderProgram* p)
                        { program_ = p; }


private:
    void init();

    QOpenGLShaderProgram *program_ = nullptr;

    QOpenGLBuffer arrayBuf;
    QOpenGLBuffer indexBuf;
};
