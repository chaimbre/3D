#pragma once

#include "datagl.h"

class CubeGL : public DataGL
{
public:
            CubeGL();
            ~CubeGL() = default;

    void    init() override;
};
