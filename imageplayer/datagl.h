#pragma once

#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

#include <vector>

class QOpenGLShaderProgram;

class DataGL : protected QOpenGLFunctions
{
public:
    virtual         ~DataGL();


    void            setProgram(QOpenGLShaderProgram* p)
                    { program_ = p; }

    virtual void    draw();
    virtual void    init() = 0;

protected:
                    DataGL();

    template <typename T>
    static void     bindAndAllocateVBO(std::vector<T>& vec,
                                       QOpenGLBuffer& vbo)
                    {
                        vbo.bind();
                        vbo.allocate(vec.data(),vec.size()*sizeof(T));
                    }

    template <typename Ver,typename Idx>
    void            bindAndAllocateVBOs(std::vector<Ver>& vertices,
                                        std::vector<Idx>& indices)
                    {
                        bindAndAllocateVBO(vertices,arrayBuf_);
                        bindAndAllocateVBO(indices,indexBuf_);
                    }

    QOpenGLBuffer   arrayBuf_;
    QOpenGLBuffer   indexBuf_;
    QOpenGLBuffer   texBuf_;

    QOpenGLShaderProgram* program_ = nullptr;
};
