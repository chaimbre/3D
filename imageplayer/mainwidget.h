#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

#include <QBasicTimer>

#include <memory>

class DataGL;
class ImageData;
class GeometryEngine;

class MainWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
            MainWidget(QWidget *par=nullptr);
    virtual ~MainWidget();

    void    setImage(const char* path);
    void    setShapeSize(int);
    void    setExageration(int);

protected:
    void    mousePressEvent(QMouseEvent*) override;
    void    mouseMoveEvent(QMouseEvent*) override;
    void    mouseReleaseEvent(QMouseEvent*) override;

    void    initializeGL() override;
    void    resizeGL(int w,int h) override;
    void    paintGL() override;

    void    initShaders();
    void    initTextures();

private:
    QOpenGLShaderProgram    program_;

    std::unique_ptr<DataGL> dataGL_;
    std::unique_ptr<QOpenGLTexture> texture_;
    std::unique_ptr<ImageData> imageData_;

    QMatrix4x4              projection_;

    QVector2D               mousePressPosition_;
    QVector3D               rotationAxis_;
    QQuaternion             rotation_;

    qreal                   angularSpeed_ = 0;
};
