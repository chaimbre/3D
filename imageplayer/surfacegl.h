#pragma once

#include "datagl.h"

#include <QImage>

struct ImageData
{
     QImage image_;

     int    shapeSize_ =1;
     int    exageration_ =1;
};


class SurfaceGL : public DataGL
{
public:
            SurfaceGL(const ImageData&);

    void    init() override;

private:
    const ImageData& imageData_;
};
